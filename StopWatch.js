var StopWatch = function() {
	//////////////////////////////////
	///           Usage            ///
	//////////////////////////////////
	/// var timer = StopWatch();   ///
	/// timer.start();             ///
	/// var time = timer.stop();   ///
	//////////////////////////////////
	///      Author: rabel001      ///
	//////////////////////////////////
	function StopWatch() {}
	StopWatch.prototype.start = function() {
		var d = new Date;
		this.BeginTime = `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}:${d.getMilliseconds()}`;
		this.start.prototype.StartTime = +new Date;
		console.time('Stopped');
		console.log("Started");
	}
	StopWatch.prototype.stop = function() {
		var d = new Date;
		this.EndTime = `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}:${d.getMilliseconds()}`;
		this.start.prototype.StopTime = +new Date;
		this.TotalMilliseconds = this.start.prototype.StopTime - this.start.prototype.StartTime;
		this.TotalSeconds = this.TotalMilliseconds / 1000;
		if(this.TotalSeconds / 60 >= 1) {
			var minutes = this.TotalSeconds / 60;
			this.Minutes = parseInt(minutes);
			this.TotalMinutes = minutes;
		}
		else {
			this.Minutes = parseInt("");
			this.TotalMinutes = parseInt("");
		}
		if(this.Minutes !== NaN) {
			this.Seconds = parseInt(this.TotalSeconds % 60);
		}
		else {
			this.Seconds = parseInt(TotalSeconds);
		}
		console.timeEnd('Stopped');
		return this;
	}
	return new StopWatch();
}