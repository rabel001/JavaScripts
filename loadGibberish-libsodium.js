/**
*
* Loads gibberish-libsodium and creates object for encrypting data.
* Created by: rabel001
* Creator site: https://gitlab.com/rabel001
*
*/

var addGibberishHelpers = () => {
	console.log('gibberish-libsodium.js loaded');
	window.gibberishLoaded = true;
	window.gibberish_helpers = {};
	var KEYLEN = sodium.crypto_secretbox_KEYBYTES;
	var MEMLIMIT = sodium.crypto_pwhash_scryptsalsa208sha256_MEMLIMIT_INTERACTIVE;
	var OPSLIMIT = sodium.crypto_pwhash_scryptsalsa208sha256_OPSLIMIT_INTERACTIVE;
	window.gibberish_helpers.encrypt = (message, password) => {
		var nonce = sodium.randombytes_buf(sodium.crypto_secretbox_NONCEBYTES);
		var salt = sodium.randombytes_buf(sodium.crypto_pwhash_scryptsalsa208sha256_SALTBYTES);
		var key = sodium.crypto_pwhash_scryptsalsa208sha256(password, salt, OPSLIMIT, MEMLIMIT, KEYLEN);
		var cipherObj = {
			nonce: sodium.to_base64(nonce), salt: sodium.to_base64(salt), keylen: KEYLEN,
			scryptM: MEMLIMIT, scryptO: OPSLIMIT,
			cipherText: sodium.to_base64(sodium.crypto_secretbox_easy(message, nonce, key))
		}
		return JSON.stringify(cipherObj);
	}

	window.gibberish_helpers.decrypt = (cipherTextJson, password) => {
		var c = JSON.parse(cipherTextJson);
		var salt = sodium.from_base64(c.salt);
		var nonce = sodium.from_base64(c.nonce);
		var key = sodium.crypto_pwhash_scryptsalsa208sha256(password, salt, c.scryptO, c.scryptM, c.keylen);
		var decoded = sodium.crypto_secretbox_open_easy(sodium.from_base64(c.cipherText), nonce, key);
		return sodium.to_string(decoded);
	}
	console.log('loadGibberish-libsodium.js loaded');
}

window.gibberishLoaded = false;
var script = document.createElement('script');
script.src = 'https://glcdn.githack.com/rabel001/JavaScripts/raw/master/gibberish-libsodium.js';
script.addEventListener('load', addGibberishHelpers);
document.head.appendChild(script);