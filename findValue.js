((global) => {
	'use strict';
	var findValue = (element, value) => {
		var specials = ["-", "[", "]", "/", "{", "}", "(", ")", "*", "+", "?", ".", "\\", "^", "$", "|"]
		var regex = new RegExp('[' + specials.join('\\') + ']', 'g');
		var escapedValue = value.replace(regex, "\\$&");
		regex = new RegExp(escapedValue);

		var allElements = element.querySelectorAll('*');
		var iframesWithErrors = [];
		var iframeErrors = false;
		var values = [];
		var innerTextMatches = [];
		var exports = new Object();
		allElements.forEach((e) => {
			if(e.tagName === "IFRAME") {
				try {
					if(!!e.contentDocument) {
						iframeErrors = false;
					}
				}
				catch {
					iframeErrors = true;
				}
				if(!iframeErrors) {
					findValue(e.contentDocument, value);
				}
				else {
					iframesWithErrors.push(e);
				}
			}
			if(e.hasAttributes()) {
				for(var prop in e.attributes) {
					if(regex.test(e.attributes[prop].value)) {
						var entry = {}
						entry[e.attributes[prop].name] = e.attributes[prop].value;
						values.push(entry);
					}
				}
			}
			try {
				var m = e.innerText.match(/"(.*?)"/g);
				if(m) {
					m.forEach((r) => {
						if(r.match(escapedValue)) {
							r = r.replace(/['"]/g, '');
							innerTextMatches.push(r);
						}
					});
				}
			}
			catch {}
		});
		if(iframesWithErrors.length > 0) exports.iframesWithErrors = iframesWithErrors;
		if(iframeErrors) exports.iframeErrors = iframeErrors;
		if(values.length > 0) exports.values = values;
		if(innerTextMatches.length > 0) exports.innerTextMatches = innerTextMatches;
		return exports;
	}
	window.findValue = findValue;
})(window);