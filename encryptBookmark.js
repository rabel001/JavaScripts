////Minified
javascript: (function () { input = prompt('password'); if (input === null) {return;}; document.head.appendChild(document.createElement("script")).src = "https://rawgit.com/rabel001/JavaScripts/master/loadGibberish-libsodium.js"; t = window.setInterval(function () { if (typeof (gibberish_helpers) !== "undefined") { var css = document.createElement('style'); css.type = 'text/css'; var cssText = document.createTextNode('#bmoverlay { position: absolute; top: 0; left: 0; z-index: 1000000; width: 100%; padding: 20px;color: white; background-color: black; font-family: Arial; } textarea { width: 60% !important; color: black !important; }'); css.appendChild(cssText); document.head.appendChild(css); enc = gibberish_helpers.encrypt(window.location.href, input); r = document.createElement("div"); r.id = 'bmoverlay'; r.innerHTML = 'Title:<br><textarea rows="1" cols="100">' + document.title + '</textarea><br>' + 'Link:<br><textarea rows="1" cols="100">' + window.location.href + '</textarea><br>' + `Script:<br><textarea rows="10" cols="100" id="txtarea">javascript: (function () { document.head.appendChild(document.createElement("script")).src = "https://rawgit.com/rabel001/JavaScripts/master/loadGibberish-libsodium.js"; timer = window.setInterval(function() { if(typeof(gibberish_helpers) !== "undefined") { try { window.bookmark = gibberish_helpers.decrypt('${enc.replace(new RegExp("\\\\","g"), "\\\\")}'.replace(new RegExp("\\\\\\\\", "g"), "\\\\"), prompt('password')); window.open(window.bookmark); } catch (err) { alert('Error ' + err.message); }; window.clearInterval(timer);}}, 1000);})();</textarea><br><div><button id="cpybtn" type="button" style="margin-right: 5px;">Copy</button><button id="bmbtn" type="button" style="margin-right: 5px;">Close</button><label id="cpylbl" style="vertical-align: bottom;"></label></div><br>`; document.body.appendChild(r); window.clearInterval(t); } }, 100); var p = window.setInterval(function () { if (!!document.getElementById('bmoverlay')) { var m = document.createElement("script"); m.text = "document.querySelector('#bmbtn').onclick = function(){document.getElementById('bmoverlay').remove();}; var cpybtn = document.querySelector('#cpybtn'); cpybtn.addEventListener('click', function(event) { var txtarea = document.querySelector('#txtarea'); txtarea.select(); try {var successful = document.execCommand('copy');var msg = successful ? 'successful' : 'unsuccessful'; document.querySelector('#cpylbl').textContent = 'Copy: ' + msg;} catch (err) {document.querySelector('#cpylbl').textContent = 'Copy: ' + msg;}});"; m.id = 'bmscript'; document.head.appendChild(m); window.clearInterval(p);} }, 100); })();

////Formatted
javascript: (function () {
	input = prompt('password');
	if (input === null) {
		return;
	};
	document.head.appendChild(document.createElement("script")).src = "https://rawgit.com/rabel001/JavaScripts/master/loadGibberish-libsodium.js";
	t = window.setInterval(function () {
		if (typeof (gibberish_helpers) !== "undefined") {
			var css = document.createElement('style');
			css.type = 'text/css'; var cssText = document.createTextNode('#bmoverlay { position: absolute; top: 0; left: 0; z-index: 1000000; width: 100%; padding: 20px;color: white; background-color: black; font-family: Arial; } textarea { width: 60% !important; color: black !important; }');
			css.appendChild(cssText); document.head.appendChild(css);
			enc = gibberish_helpers.encrypt(window.location.href, input);
			r = document.createElement("div");
			r.id = 'bmoverlay';
			r.innerHTML = 'Title:<br>' 
			+ '<textarea rows="1" cols="100">' + document.title + '</textarea><br>' 
			+ '<Link:<br><textarea rows="1" cols="100">' + window.location.href + '</textarea><br>'
			+ 'Script:<br>'
			+ '<textarea rows="10" cols="100" id="txtarea">'
			+ 'javascript: (function() {'
				+ 'input = prompt("password"); if (input === null) {return;};'
				+ 'document.head.appendChild(document.createElement("script")).src = "https://rawgit.com/rabel001/JavaScripts/master/loadGibberish-libsodium.js";'
				+ 'timer = window.setInterval(() => {'
					+ 'if (typeof(gibberish_helpers) !== "undefined") {'
						+ 'try {'
							+ `window.bookmark = gibberish_helpers.decrypt('${enc.replace(/\\\\/g, "\\\\")}'.replace(/\\\\/g, "\\\\\\\\"), input);`
							+ 'window.open(window.bookmark);'
						+ '}'
						+ 'catch (err) {'
							+ 'alert("Error " + err.message);'
						+ '};'
						+ 'window.clearInterval(timer);'
					+ '}'
					+ '}, 1000);'
				+ '})();'
			+ `</textarea><br>
			<div>
				 <button id="cpybtn" type="button" style="margin-right: 5px;">Copy</button>
				 <button id="bmbtn" type="button" style="margin-right: 5px;">Close</button>
				 <label id="cpylbl" style="vertical-align: bottom;"></label>
			</div><br>`;
			r.style.fontFamily = "Arial";
			document.body.appendChild(r);
			window.clearInterval(t);
		}
	}, 100);
	var p = window.setInterval(function () {
		if (!!document.getElementById('bmoverlay')) {
			var m = document.createElement("script");
			m.id = 'bmscript';
			m.text = `document.querySelector('#bmbtn').onclick = () => {
				document.getElementById('bmoverlay').remove();
			};
			var cpybtn = document.querySelector('#cpybtn');
			cpybtn.addEventListener('click', () => {
				var txtarea = document.querySelector('#txtarea');
				txtarea.select();
				try {
					var successful = document.execCommand('copy');
					var msg = successful ? 'successful' : 'unsuccessful';
					document.querySelector('#cpylbl').textContent = 'Copy: ' + msg;
				} 
				catch(err) {
					document.querySelector('#cpylbl').textContent = 'Copy: ' + msg;
				}
			});`
			document.head.appendChild(m);
			window.clearInterval(p);
		}
	}, 100);
})();