/*--- Usage ----
  //No jQuery required. Use javascript selectors like 'document.querySelectorAll('.classname')';
	
  waitForElements("element.ClassOrId, callBackFunction);
  
	//Example
	function theCallbackFunction(jNode) {
		jNode.innerText = "This comment was added by waitForElements";
	}
	waitForElements('div.classname', theCallbackFunction);
*/

function waitForElements(selectorTxt, actionFunction) {
	var targetNodes = document.querySelectorAll(selectorTxt);
	if (targetNodes && targetNodes.length > 0) {
		window.btargetsFound = true;
		targetNodes.forEach(function (e) {
			var jThis = e;
			var alreadyFound = !!e.getAttribute('found');
			if (!alreadyFound) {
				var cancelFound = actionFunction(jThis);
				if (cancelFound) {
					window.btargetsFound = false;
				}
				else {
					jThis.setAttribute('found', true);
				}
			}
		});
	}
	else {
		window.btargetsFound = false;
	}
	var controlObj = waitForElements.controlObj || {};
	var controlKey = selectorTxt.replace(/[^\w]/g, '_');
	var timeControl = controlObj[controlKey];
	if (btargetsFound && timeControl) {
		clearInterval(timeControl);
		delete controlObj[controlKey];
		console.log('Cleared');
	}
	else {
		if (!timeControl) {
			timeControl = setInterval(function () {
				waitForElements(selectorTxt, actionFunction);
			}, 300);
			controlObj[controlKey] = timeControl;
		}
	}
	waitForElements.controlObj = controlObj;
}